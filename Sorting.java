import java.util.Scanner;
import java.util.Arrays;
import java.util.Date;

//
public class Sorting {

	public static void  main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean isrunning = true;
		//System.out.println("Let's sort something...i guess");
		//System.out.println("");
		printdatachoices();


		int datatype = sc.nextInt();
		System.out.println("Enter size of your array");

		int arraysize = sc.nextInt();
		int a[] = new int[arraysize];
		//choose array size
		switch (datatype)
		{

		case 1:
		a = InOrder(arraysize);
		break;
		case 2:
		a = ReverseOrder(arraysize);
		break;
		case 3:
		a = AlmostOrder(arraysize);
		break;
		case 4:
		a = RandomOrder(arraysize);
		break;

		}
		//testprint(a);
		//output sorting method choices
//main loop for running everything
		do
		{
			int[] testarray = Arrays.copyOf(a,arraysize);
			int[] counterarr = new int [3];
			int numruntimes;
			long time;
			long avgtime;
			printsortchoices();
			int sortingtype = sc.nextInt();
			if (sortingtype !=0)
			{
			System.out.println("How many times would you like to run the test?");
			numruntimes = sc.nextInt();

			}
			else
			{ numruntimes =1;}
			long[] temptime = new long[numruntimes];

			for(int strtcnt =0;strtcnt<numruntimes;strtcnt++)
			{
			switch (sortingtype)
			{

			//insertion sort
			case 1:
			temptime[strtcnt] = InsertionSort(testarray, counterarr);
			//testprint(testarray);
			counterprint(counterarr,temptime[strtcnt]);
			testarray = Arrays.copyOf(a,arraysize);
			//time += temptime;
			break;
			//selection
			case 2:
				temptime[strtcnt] = SelectionSort(testarray, counterarr);
				//testprint(testarray);
				counterprint(counterarr,temptime[strtcnt]);
				testarray = Arrays.copyOf(a,arraysize);
				break;
			//radix
			case 3:
				temptime[strtcnt] = RadixSort(testarray, counterarr,arraysize);
				//testprint(testarray);
				counterprint(counterarr,temptime[strtcnt]);
				testarray = Arrays.copyOf(a,arraysize);
			break;
			//heapsort
			case 4:
				temptime[strtcnt] = HeapSort(testarray, counterarr);
				//testprint(testarray);
				counterprint(counterarr,temptime[strtcnt]);
				testarray = Arrays.copyOf(a,arraysize);
				break;
				//mergesort
			case 5:
				int temparr[] = new int[arraysize];
				temptime[strtcnt] = MergeSort(testarray, counterarr, temparr, 0,arraysize);
				//testprint(testarray);
				counterprint(counterarr,temptime[strtcnt]);
				testarray = Arrays.copyOf(a,arraysize);
				break;
				//Simple Quick Sort
			case 6:
				temptime[strtcnt] = QuickSort(testarray, counterarr);
				//testprint(testarray);
				counterprint(counterarr,temptime[strtcnt]);
				testarray = Arrays.copyOf(a,arraysize);
				break;
			case 0:
				isrunning = false;
				temptime[0] =0;
				break;

			}

			}

			avgtime = temptime[0];
			for(int i =1 ;i<numruntimes;i++)
			{

			avgtime = avgtime+temptime[i];

			}
			//calc avg and format output
			avgtime = avgtime/(long)numruntimes;
			long millis = avgtime/ 1000000;
			long decimal = avgtime% 1000000;
			System.out.print("Average Time (ms): " + millis +".");
			System.out.format("%06d%n",decimal);



			//sortingtype = sc.nextInt();

		}
		while(isrunning);
		//output
	}


	public static void counterprint(int counterarr[], long time)
	{
		System.out.println("Comparisons:" + counterarr[0]);
		System.out.println("Movements:" + counterarr[1]);
		System.out.println("Total Time:" + time);
	}

	private static int partition(int testarray [], int counterarr[], int first, int last)
	{	counterarr[1]++;
		int pivot = testarray[first];
		int low = first +1;
		int high = last;
		//quick sort with first element as pivot point...
		while (high > low)
		{//search forward
			while (low <= high && testarray[low] <= pivot)
			{		low++;
			counterarr[0]++;}
		//search backward
			while (low <= high && testarray[high] > pivot)
				{high--;
				counterarr[0]++;}
		//swap
			if(high > low)
			{
				counterarr[1]++;
			int temp = testarray[high];
			counterarr[1]++;
			testarray[high] = testarray[low];
			counterarr[1]++;
			testarray[low] = temp;
			}
		}

		while (high > first && testarray[high] >= pivot)
		      {high--;
		      counterarr[0]++;}
		//Swap pivot [high]
		counterarr[0]++;
		if (pivot > testarray[high]) {
		      testarray[first] = testarray[high];
		      counterarr[1]++;
		      testarray[high] = pivot;
		      counterarr[1]++;
		return high;
		}
		else
		{
		return first;
		}
		}


	public static void QuickSortHelper(int testarray[],int counterarr[],int low, int high)
	{
		//counterarr[0]++;
		//recursive calls
		if (high > low)
		{
			int pivotIn = partition(testarray, counterarr, low, high);
			QuickSortHelper(testarray, counterarr, low, pivotIn-1);
			QuickSortHelper(testarray, counterarr, pivotIn + 1, high);


		}



	}

	public static long QuickSort(int testarray[],int counterarr[])
	{	long starttime = System.nanoTime();
		counterarr[0]=0;
		counterarr[1]=0;
		int arraysize = testarray.length;
		/*sorting!
		 */
		QuickSortHelper(testarray, counterarr, 0, arraysize - 1);





		long stoptime = System.nanoTime();
	    //System.out.println(stoptime);
	    long totaltime = stoptime - starttime;
     return totaltime;
	}


	public static void MaxHeap(int i,int n,int counterarr[],int testarray[])
	{
			int max = i;
	        int left=2*i+1;
	        int right=2*i+2;
	        //MaxHeap function....needed by other function...
	    	//counterarr[0]++;
	    	counterarr[0]++;
	    	//decided to count this who expression as 1 comparison...
	        if(left < n && testarray[left] > testarray[max]) max = left;
	        //counterarr[0]++;
	        counterarr[0]++;
	        if(right < n && testarray[right] > testarray[max]) max = right;
	        counterarr[0]++;
	        if (max != i) {  // node i is not maximal
	            int temp = testarray[i];
		    	counterarr[1]++;
		    	testarray[i] = testarray[max];
		    	counterarr[1]++;
		    	testarray[max] = temp;
		    	counterarr[1]++;
	            MaxHeap(max, n, counterarr,testarray);
	        }
	}

	public static void Merge(int testarray[],int counterarr[],int temparr[], int low, int middle, int high)
	{
		//int size = testarray.length;
		//merge everything...
		//copy first part
        for (int q = low; q < middle; q++)
       { counterarr[1]++;


        	temparr[q] = testarray[q];

        	}

        int q = low;
        int j = middle;
        int k = low;
        //counterarr[0]++;
        //counterarr[0]++;
        while (q < middle && j < high)
        {	counterarr[0]++;
        if (temparr[q] <= testarray[j])
        	{counterarr[1]++;
        	testarray[k++] = temparr[q++];
        	}
        	else
        	{ counterarr[1]++;
        		testarray[k++] = testarray[j++];
        	}
        	}
        while (q < middle)
        	{//counterarr[0]++;
            counterarr[1]++;
        	testarray[k++] = temparr[q++];}

	}
	public static void MergeSortHelper(int testarray[],int counterarr[], int temparr[], int low, int high)
	{
		//counterarr[0]++;
		if (low < high-1) {
			//index of middle
	          int middle = (high + low) / 2;
	          //sort the left
	          MergeSortHelper(testarray, counterarr, temparr, low, middle);
	          //sort the right
	          MergeSortHelper(testarray, counterarr, temparr, middle, high);
	          //combine both
	          Merge(testarray, counterarr, temparr, low, middle, high);


		}

	}
	public static long MergeSort(int testarray[], int counterarr[], int[] temparr, int l, int h)
	{
	counterarr[0]=0;
	counterarr[1]=0;



	int arraysize = testarray.length;

	//temp = testarray[0];
	//testarray[0] = testarray[arraysize];
	//testarray[arraysize] = temp;

	long starttime = System.nanoTime();



	MergeSortHelper(testarray, counterarr,temparr,l,h);


	 long stoptime = System.nanoTime();
	    //System.out.println(stoptime);
	    long totaltime = stoptime - starttime;
     return totaltime;

	}
	public static long HeapSort(int testarray[],int counterarr[])
	{
	counterarr[0]=0;
	counterarr[1]=0;

	//int temp;
	int arraysize = testarray.length;

	//temp = testarray[0];
	//testarray[0] = testarray[arraysize];
	//testarray[arraysize] = temp;

	long starttime = System.nanoTime();
	//build the heap...
    for (int i=arraysize/2; i>=0; i--) MaxHeap(i, arraysize,counterarr,testarray);

    for (int i=arraysize-1; i>0; i--)
    {
    	//move max to i
    	int temp = testarray[0];
    	counterarr[1]++;
    	testarray[0] = testarray[i];
    	counterarr[1]++;
    	testarray[i] = temp;
    	counterarr[1]++;
    	//fix heap
    	MaxHeap(0,i,counterarr,testarray);

    }




	 long stoptime = System.nanoTime();
	    //System.out.println(stoptime);
	    long totaltime = stoptime - starttime;
     return totaltime;

	}
	public static long RadixSort(int testarray[], int counterarr[],int arraysize)
	{
		counterarr[0]=0;
		counterarr[1]=0;
    	int[] output = new int[arraysize];


		long starttime = System.nanoTime();

			for(int plce=1; plce <=100000000;plce *= 10)
			{

		    int[] count = new int[arraysize];

		    for(int i=0; i < arraysize; i++)
		    {
		    	int digit = grabDigit(testarray[i],plce);
		    	count[digit] +=1;
            	counterarr[1]++;

		    }
		    for(int i=1; i < arraysize; i++)
		    {
		    	count[i] +=count[i-1];
            	counterarr[1]++;

		    }

		    for(int i = arraysize - 1; i>=0; i--)
		    {
		    	int digit = grabDigit(testarray[i],plce);
		    	output[count[digit]-1] = testarray[i];
            	counterarr[1]++;
		    	count[digit]--;
            	counterarr[1]++;


			}

		    //move the sorted array back to the original test array....
		    for(int i=0; i < arraysize; i++)
		    {
		    	testarray[i] =output[i];
            	counterarr[1]++;

		    }



	}

			  long stoptime = System.nanoTime();
			    //System.out.println(stoptime);
			    long totaltime = stoptime - starttime;
		        return totaltime;
	}
	public static int grabDigit(int val, int digPlce)
	{
		return ((val/digPlce) % 10);
	}

	public static long SelectionSort(int testarray[], int counterarr[])
	{
		counterarr[0]=0;
		counterarr[1]=0;
		long starttime = System.nanoTime();
		//test
	    for(int i=0; i<testarray.length; i++)
	    {
	    	//set min index
	    	int min_index = i;
	    	for(int j = i; j<testarray.length; j++)
	    	{
            	counterarr[0]++;
	    		if(testarray[min_index] >testarray[j])
	    		{
	    			min_index = j;
	    			counterarr[1]++;

	    		}
	    	}
	    	//swap stuff
	    	int temp = testarray[i];
	    	counterarr[1]++;
	    	testarray[i] = testarray[min_index];
	    	counterarr[1]++;
	    	testarray[min_index] = temp;
	    	counterarr[1]++;

	    }


	    long stoptime = System.nanoTime();
	    //System.out.println(stoptime);
	    long totaltime = stoptime - starttime;
        return totaltime;
	}
	public static long InsertionSort(int testarray[], int counterarr[])
	{
		int tmpint = 0;
		counterarr[0]=0;
		counterarr[1]=0;
		long starttime = System.nanoTime();
	    for (int q = 1; q < testarray.length; q++) {
            for(int j = q ; j > 0 ; j--){
            	counterarr[0]++;
                if(testarray[j] < testarray[j-1]){
                    //swap stuff
                	tmpint = testarray[j];
                    counterarr[1]++;
                    testarray[j] = testarray[j-1];
                    counterarr[1]++;
                    testarray[j-1] = tmpint;
                    counterarr[1]++;
                }
            }
        }
	    long stoptime = System.nanoTime();
	    //System.out.println(stoptime);
	    long totaltime = stoptime - starttime;
        return totaltime;
	}
	public static void printdatachoices()
	{	System.out.println("Choose how the elemenents are initially ordered in the array ");
		System.out.println("1 - InOrder");
			System.out.println("2 - ReverseOrder:");
			System.out.println("3 - AlmostOrder:");
			System.out.println("4 - Random:");

	}
	public static void printsortchoices()
	{	System.out.println("Choose how your data is sorted ");
		System.out.println("1 - insertion sort:");
			System.out.println("2 - selection sort:");
			System.out.println("3 - radix sort:");
			System.out.println("4 - heap sort:");
			System.out.println("5 - merge sort:");
			System.out.println("6 - quick sort(simple):");
			System.out.println("0 - quit");
	}
	public static int[] InOrder(int arraysize)
	{
		int A [] = new int[arraysize];
		for ( int i = 0 ; i < A.length-1 ; i++) {
		      A [i] = (int) (Math.random () * 100);
		     }
		Arrays.sort(A);
		return A;
	}
	public static int[] RandomOrder(int arraysize)
	{
		int A [] = new int[arraysize];
		for ( int i = 0 ; i < A.length-1 ; i++)
		{
		 A [i] = (int) (Math.random () * 100);
		     }
		return A;
	}

	public static int[] ReverseOrder(int arraysize)
	{
		int A[] = InOrder(arraysize);
		for(int i=0;i<A.length;i++)
		    A[i]=-A[i];
		Arrays.sort(A);
		for(int i=0;i<A.length;i++)
		    A[i]=-A[i];
		return A;
	}

	public static int[] AlmostOrder(int arraysize)
	{
		int A [] = new int[arraysize];
		for ( int i = 0 ; i < A.length-1 ; i++) {
		      A [i] = (int) (Math.random () * 100);
		     }
		Arrays.sort(A);

		int numswaps = A.length / 3;
		if (numswaps == 0)
		numswaps = 1;
		for(int i=0, m=(A.length-1);i<numswaps;i++,m--)
		{
			int temp = 0;
			temp = A[i];
			A[i] = A[m];
			A[m] = temp;

		}

		return A;
	}
	public static void testprint(int nums[])
	{
		for(int i=0;i<nums.length;i++)
		{
			System.out.println(nums[i]);

		}
		}
	}






